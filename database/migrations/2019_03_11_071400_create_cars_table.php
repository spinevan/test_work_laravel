<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            //$table->integer('brand_id')->unsigned();
            $table->integer('model_id')->unsigned();

            $table->Integer('km');
            $table->string('color');
            $table->smallInteger('owners');
            $table->string('vin')->unique();
            $table->smallInteger('power');
            $table->decimal('engineCapacity',5,1);
            $table->string('transmission');
            $table->boolean('is_published')->default(false);

            //этих полей нет файле импорта
            $table->decimal('price',15,2)->default(0);
            $table->smallInteger('year')->default(1900);
            $table->string('body_type')->default('Седан');
            ///////

            //$table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('model_id')->references('id')->on('car_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
