<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ImportUsedCars;

class ImportUsedCarsCommand extends Command
{

    protected $ImportUsedCars;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:used-cars {source}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импорт бу автомобилей';


    /**
     * Create a new command instance.
     *
     * @var string
     * @return void
     */
    public function __construct(ImportUsedCars $importUsedCars)
    {
        parent::__construct();

        $this->ImportUsedCars = $importUsedCars;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $source = $this->argument('source');

        $this->info('Запущен импорт бу авто источник: '.$source);

        //загружаем массив авто для импорта

        $res = $this->ImportUsedCars->getArrImport($source);
        if (!is_array($res) ){
            $this->info($res);
            return;
        }

        $this->info('элементов для импорта '.count($res));

        //обновляем\добавляем, // решил для простоты просто обновлять или добавлять все ам, без проверки на изменение данных

        $publishedAuto = array();

        foreach ($res as $item) {
            $resultImport = $this->ImportUsedCars->createUpdateAuto($item);
            $this->info($resultImport);
            $publishedAuto[] = $item->vin;
        }

        //снимаем с публикации то чего нет нет
        $resUnPublish = $this->ImportUsedCars->unPublishCars($publishedAuto);

        if($resUnPublish){
            $this->info('Выполнено снятие с публикации');
        }

    }
}
