<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ImportUsedCars;


class ImportUsedCarsServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ImportUsedCarsService', function ($app){
            return new ImportUsedCars();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }

    public function provides()
    {
        return ['App\Services\ImportUsedCars'];
    }
}
