<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    protected $fillable = ['name'];

    use CrudTrait;

    public function brand(){
        return $this->belongsTo('App\Models\Brand');
    }

}
