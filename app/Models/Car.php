<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['km', 'color', 'owners', 'vin', 'power', 'engineCapacity', 'transmission', 'is_published', 'price', 'year', 'body_type'];

    use CrudTrait;

    public function carModel(){
        return $this->belongsTo('App\Models\CarModel', 'model_id');
    }

}
