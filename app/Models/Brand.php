<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['name'];
    use CrudTrait;

    public function models(){
        return $this->hasMany('App\Models\CarModel');
    }

}
