<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CarRequest as StoreRequest;
use App\Http\Requests\CarRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CarCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Car');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/car');
        $this->crud->setEntityNameStrings('car', 'cars');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */


        //$this->crud->setFromDb();

        $this->crud->addColumn(['name' => 'vin',
            'label' => "vin"]);

        $this->crud->addColumn(['name' => 'model_id',
            'label' => "model",
            'entity' => 'carModel',
            'attribute' => "name",
            'model' => "App\Models\CarModel",
            'type' => "select",]);

        $this->crud->addColumn(['name' => 'km',
            'label' => "km"]);
        $this->crud->addColumn(['name' => 'color',
            'label' => "color"]);
        $this->crud->addColumn(['name' => 'owners',
            'label' => "owners"]);
        $this->crud->addColumn(['name' => 'power',
            'label' => "power"]);
        $this->crud->addColumn(['name' => 'engineCapacity',
            'label' => "engineCapacity"]);
        $this->crud->addColumn(['name' => 'transmission',
            'label' => "transmission"]);
        $this->crud->addColumn(['name' => 'price',
            'label' => "price"]);
        $this->crud->addColumn(['name' => 'year',
            'label' => "year"]);
        $this->crud->addColumn(['name' => 'is_published',
            'label' => "Опубликовано",
            'type' => 'check',]);
        $this->crud->addColumn(['name' => 'body_type',
            'label' => "body_type"]);

        // add asterisk for fields that are required in CarRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->allowAccess('show');
        $this->crud->denyAccess('update');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
