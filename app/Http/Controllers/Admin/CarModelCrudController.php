<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CarModelRequest as StoreRequest;
use App\Http\Requests\CarModelRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CarModelCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CarModelCrudController extends CrudController
{


    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\CarModel');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/carmodel');
        $this->crud->setEntityNameStrings('carmodel', 'Модели автомобилей');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */



        $this->crud->addColumn(['name' => 'brand_id',
            'label' => "Бренд",
            'entity' => 'brand',
            'attribute' => "name",
            'model' => "App\Models\Brand",
            'type' => 'select',]);

        $this->crud->addColumn(['name' => 'name',
            'label' => "Модель"]);

        // add asterisk for fields that are required in CarModelRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->crud->allowAccess('show');
        $this->crud->denyAccess('update');


    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
