<?php

namespace App\Http\Controllers\API;


use App\Http\Requests\API\CarFilterRequest;
use App\Models\Car;
use App\Models\CarModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class CarController extends Controller
{
    public function filter(Request $request, Car $car, CarModel $carModel){

       //не получилось использовать App\Http\Requests\API\CarFilterRequest, инпут пустой при использовании filter(CarFilterRequest $request)

        $validator = Validator::make($request->all(),
            ['brand' => 'filled|max:255|alpha_dash',
                'price_from' => 'integer',
                'price_to' => 'integer',
                'year_from' => 'integer',
                'year_to' => 'integer',
                'body_type' => 'filled|max:255|alpha_dash',]);

        if($validator->fails())
            return $validator->errors();



        $carQuery = $car->newQuery();


        if ($request->has('price_from')){
            $carQuery->where('price', '>=', $request->input('price_from'));
        }

        if ($request->has('price_to')){
            $carQuery->where('price', '<=', $request->input('price_to'));
        }

        if ($request->has('year_from')){
            $carQuery->where('year', '>=', $request->input('year_from'));
        }

        if ($request->has('year_to')){
            $carQuery->where('year', '<=', $request->input('year_to'));
        }

        if ($request->has('body_type')){
            $carQuery->where('body_type', '=', $request->input('body_type'));
        }


        if ($request->has('brand')){

            //
            $arrIdModels = $carModel->newQuery()
                ->whereHas('brand', function ($queryBrand) use ($request){
                    $queryBrand->where('name', $request->input('brand'));
                })->get('id');


            $carQuery->whereHas('carModel',
                function ($queryModel) use ($arrIdModels){
                    $queryModel->whereIn('id', $arrIdModels);
                });


        }


        $carQuery->where('is_published', 1);

        return $carQuery
            ->join('car_models', 'cars.model_id', '=', 'car_models.id')
            ->select('cars.id',
                'car_models.name AS Model',
                'cars.km',
                'cars.color',
                'cars.owners',
                'cars.vin',
                'cars.power',
                'cars.engineCapacity',
                'cars.transmission',
                'cars.price',
                'cars.year',
                'cars.body_type')
            ->get();

    }
}
