<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 12.03.19
 * Time: 9:35
 */

namespace App\Services;

use App\Models\Brand;
use App\Models\CarModel;
use App\Models\Car;

class ImportUsedCars
{

    /**
     * @param $path string предполагается что файлик будет всегда лежать в папке public/uploads
     * @return array|mixed|string
     */

    public function getArrImport($path){


        if (filter_var($path, FILTER_VALIDATE_URL)===false ){
            //проверяем файл и читаем json из файла
            $path = public_path() .'/uploads/'. $path;

            if (file_exists($path)){

                $fileContent = file_get_contents($path);
                $data = json_decode($fileContent);
                return $data;

            }else{
                return 'ERROR: Файл не существует!';
            }

        }else{

            $fileContent = @file_get_contents($path);
            $data = json_decode($fileContent);
            return $data;
        }

    }


    /**
     * @param $item object
     * @return string
    */
    public function createUpdateAuto($item){


        //$car = Car::firsOrNew(['vin' => $item->vin]);

        $car = Car::where('vin', '=', $item->vin)->first();

        if (empty($car)){
            $car = new Car((array) $item);
        }else{
            $car->fill((array) $item);
        }

        $car->is_published = true;


        if (empty($car->carModel)){
            $car->model_id = $this->getCarModel($item->model)->id;
        }

        //dd($car);

        $result = $car->save();

        if ($result){
            return $car->vin.' - ok';
        }else{
            return $item->vin.' - ERROR';
        }


    }

    private function getBrand(){

        $brand = Brand::first();
        if (empty($brand)){
            $brand = new Brand();
            $brand->name = 'Hyundai';
            $brand->save();
        }

        return $brand;

    }

    private function getCarModel($nameModel){

        $carModel = CarModel::where('name', '=', $nameModel)->first();

        if (empty($carModel)){
            $carModel = new CarModel();
            $carModel->name = $nameModel;
            $carModel->brand_id = $this->getBrand()->id;
            $carModel->save();
        }

        return $carModel;


    }

    public function unPublishCars($arrVinPublished){

        if(count($arrVinPublished)) {

            $result = Car::where('is_published', '=', 1)
                ->whereNotIn('vin', $arrVinPublished)
                ->update(['is_published' => 0]);
            return $result;
        }

        return false;
    }


}